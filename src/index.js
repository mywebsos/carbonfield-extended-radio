/**
 * External dependencies.
 */
import { registerFieldType } from "@carbon-fields/core";

/**
 * Internal dependencies.
 */
import "./style.scss";
import ExtendedRadioField from "./main";

registerFieldType("extended_radio", ExtendedRadioField);
