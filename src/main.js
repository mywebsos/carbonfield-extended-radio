/**
 * External dependencies.
 */
import RadioField from "../vendor/htmlburger/carbon-fields/packages/core/fields/radio";

class ExtendedRadioField extends RadioField {}

export default ExtendedRadioField;
