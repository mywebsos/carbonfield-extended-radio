<?php

use Carbon_Fields\Carbon_Fields;
use Carbon_Field_Extended_Radio\Extended_Radio_Field;

define( 'Carbon_Field_Extended_Radio\\DIR', __DIR__ );

Carbon_Fields::extend( Extended_Radio_Field::class, function( $container ) {
	return new Extended_Radio_Field(
		$container['arguments']['type'],
		$container['arguments']['name'],
		$container['arguments']['label']
	);
} );
