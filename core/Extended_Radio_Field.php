<?php
declare(strict_types=1);

namespace Carbon_Field_Extended_Radio;

use Carbon_Fields\Field\Radio_Field;

/**
 * Class Extended_Radio_Field
 *
 * Custom field type for Carbon Fields library that extends the built-in Radio Field.
 * It allows for additional options and functionality.
 *
 * @package Carbon_Field_Extended_Radio
 */
class Extended_Radio_Field extends Radio_Field {

	/**
	 * The format in which the field value should be returned.
	 *
	 * @var string
	 */
	protected string $return_format = 'value';

	/**
	 * Prepare the field type for use.
	 * Called once per field type when activated.
	 *
	 * @static
	 * @access public
	 *
	 * @return void
	 */
	public static function field_type_activated(): void {
		$dir = \Carbon_Field_Extended_Radio\DIR . '/languages/';
		$locale = get_locale();
		$path = $dir . $locale . '.mo';
		load_textdomain( 'carbon-field-extended-radio', $path );
	}

	/**
	 * Enqueue scripts and styles in admin.
	 * Called once per field type.
	 *
	 * @static
	 * @access public
	 *
	 * @return void
	 */
	public static function admin_enqueue_scripts(): void {
		$root_uri = \Carbon_Fields\Carbon_Fields::directory_to_url( \Carbon_Field_Extended_Radio\DIR );

		// Enqueue field styles.
		wp_enqueue_style( 'carbon-field-extended-radio', $root_uri . '/build/bundle.css' );

		// Enqueue field scripts.
		wp_enqueue_script( 'carbon-field-extended-radio', $root_uri . '/build/bundle.js', array( 'carbon-fields-core' ) );
	}

	/**
	 * Get the formatted value of the field.
	 * 
	 * @return array
	 */
	public function get_formatted_value(): array {
		$options = $this->get_options();
		$options_values = $this->get_options_values();

		if ( empty( $options_values ) ) {
			$options_values[] = '';
		}
		
		$key = $this->get_value();
		$key = $this->get_values_from_options( array( $key ) );
		$key = ! empty( $key ) ? $key[0] : $options_values[0];

		if( 'value' === $this->return_format ) {
			return $options[ $key ];
		} elseif( 'array' === $this->return_format ) {
			return array( $key => $options[ $key ] );
		} else {
			return $key;
		}
	}

	/**
	 * Set the format in which the field value should be returned.
	 *
	 * @param string $return_format The format in which the field value should be returned.
	 *
	 * @return $this
	 */
	public function return_format(string $return_format): self {
		$this->return_format = $return_format;
		return $this;
	}

	/**
	 * Returns the field value as a JSON-encoded string.
	 * Modify the field properties that are passed to the React component which represents the field.
	 *
	 * @param bool $load Should the value be loaded from the database or use the current value.
	 *
	 * @return array
	 */
	public function to_json($load) {
		$field_data = parent::to_json( $load );
		$options = $this->parse_options( $this->get_options(), true );
		$options_values = $this->get_options_values();

		if ( empty( $options_values ) ) {
			$options_values[] = '';
		}

		$value = $this->get_value();
		$value = $this->get_values_from_options( array( $value ) );		
		$value = ! empty( $value ) ? $value[0] : $options_values[0];

		$field_data = array_merge( $field_data, array(
			'options' => $options,
			'value' => $value,
		) );

		return $field_data;
	}
}
